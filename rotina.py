# -*- coding: utf-8 -*-
import sys
if sys.version_info[0] == 3:
    # for Python3
    from tkinter import *
else:
    # for Python2
    from Tkinter import *
from tkinter import filedialog


from pytz import timezone
import pytz

import os
import psycopg2
import time
import datetime
import threading as thread
import math
import copy
import matplotlib.pyplot as plt
import numpy as np
import ctypes

###########################################################
class Coleta:

	#Retorna quantidade de coletas que possuem: 
	#datahora_col = dtHR (YYYY-MM-DD HH:mm:ss) e cidade = city
	@staticmethod 
	def selectColeta(city, dtHr):
		con = Connection.getConnection()
		cur = con.cursor()
		cur.execute("SELECT count(*) FROM coleta WHERE datahora_col = %s AND cidade = %s",[dtHr,city])
		resultSet = cur.fetchall()
		cont = 0
		for row in resultSet:
			cont = row[0]
			print(dtHr)
			print(cont)
		cur.close()
		con.close()
		return cont

	#Inserção no banco de uma coleta
	#Todos os campos a não ser datahora_col, cidade, insercaomanual e validacao são nulos
	#datahora_col = dtHR, cidade = city, insercaomanual = True, validacao = False
	@staticmethod
	def insertColeta(city, dtHr):
		con = Connection.getConnection()
		cur = con.cursor()
		args_str = cur.mogrify("(%s,%s,%s,%s)", (dtHr, city, True, False)).decode('utf8')
		#print("INSERT INTO coleta (datahora_col, cidade, insercaomanual, validacao) VALUES "+args_str)
		cur.execute("INSERT INTO coleta (datahora_col, cidade, insercaomanual, validacao) VALUES "+args_str)
		con.commit()
		cur.close()
		con.close()

###########################################################
class Spectrum:

	#Construtor
	#Entrada: dtHr, city, curve
	#dtHr (YYYY-MM-DD HH:mm:ss)
	#self.curve é uma lista de lista [compOnda, Irradiância]
	#Assimila a entrada a self.dtHr, self.city, self.curve
	def __init__(self, dtHr, city, curve):
		self.dtHr = dtHr
		self.city = city
		self.curve = curve

	#Insere todos os dados armazenados nos atributos no BD
	def insertSpectrum(self):
		con = Connection.getConnection()
		cur = con.cursor()
		args_str = ','.join(cur.mogrify("(%s,%s,%s,%s)", (self.dtHr, self.city,x[0],x[1])).decode('utf8') for x in self.curve)
		#print("INSERT INTO espectro (datahora_espec, cidade, componda, irradiancia) VALUES "+args_str)
		cur.execute("INSERT INTO espectro (datahora_espec, cidade, componda, irradiancia) VALUES "+args_str)
		con.commit()
		con.close()

	#Cria 3 threads para verificar quais interpolações de self.curve ainda não foram cadastradas no BD
	#Cada thread executa o método Spectrum.filterSubcurve para alguma sublista de self.curve
	#No final self.curve passa a ter somente as interpolações ainda não cadastradas
	def filterCurve(self):
		threads = []
		filtrated = []

		qtdThreads = 3
		blockSize = math.ceil(len(self.curve)/qtdThreads)
		pointer = 0

		for i in range(qtdThreads):
			if i == qtdThreads - 1:
				threads.append(thread.Thread(target=Spectrum.filterSubCurve, args=(self.dtHr, self.city, self.curve[pointer:], filtrated)))
			else:
				threads.append(thread.Thread(target=Spectrum.filterSubCurve, args=(self.dtHr, self.city, self.curve[pointer:pointer+blockSize],filtrated)))
			pointer += blockSize			
			threads[i].start()

		for i in threads:
			i.join()

		self.curve = filtrated
		
	#Verifica quais interpolações de subCurve ainda não foram cadastradas no BD e as salva em filtrated
	#Retorna filtrated
	@staticmethod
	def filterSubCurve(dtHr, city, subCurve, filtrated):
		con = Connection.getConnection()
		cur = con.cursor()
		copySubCurve = copy.deepcopy(subCurve) 
		for i in copySubCurve:
			cur.execute("SELECT * FROM espectro WHERE datahora_espec = %s AND cidade = %s AND compOnda = %s",[dtHr,city,i[0]])
			if cur.rowcount > 0:
				subCurve.remove(i)
		cur.close()
		con.close()
		filtrated += subCurve

	#Dado um waveLen, irrad, city e dthr, insere esses valores como uma tupla na tabela espectro do BD
	@staticmethod
	def insertInterpolation(waveLen, irrad, city, dthr):
		con = Connection.getConnection()
		cur = con.cursor()
		cur.execute("insert into espectro(datahora_espec, cidade, compOnda, Irrad) values (%s,%s,%s,%s)",[dthr,city,waveLen,irrad])
		con.commit()
		cur.close()
		con.close()

	#Requisição de uma coleta espectral no BD com valor de data e hora como dtHr e cidade como city
	#Retorno são duas listas: x (referente ao comprimento de onda) e y (referente a irrandiância)
	#No qual uma interpolações seria formada como (x[i],y[i])
	@staticmethod
	def selectSpectrum(city, dtHr):
		con = Connection.getConnection()
		cur = con.cursor()
		print("SELECT compOnda, irradiancia FROM espectro WHERE datahora_espec = %s AND cidade = %s",[dtHr,city])
		cur.execute("SELECT compOnda, irradiancia FROM espectro WHERE datahora_espec = %s AND cidade = %s",[dtHr,city])
		resultSet = cur.fetchall()
		x = []
		y = []
		for row in resultSet:
			x.append(row[0])
			y.append(row[1])
		cur.close()
		con.close()
		return x,y

	#Requisição de uma coleta espectral no BD dentro do intervalo de tempo formado por dtTm1 e dtTm2 referente a cidade = city
	#Retorno são duas listas: x (referente ao comprimento de onda) e y (referente a irrandiância)
	#Interpolações seriam (x[i],y[i])
	@staticmethod
	def selectSpectrumBetweenInterval(city, dtTm1, dtTm2):
		con = Connection.getConnection()
		cur = con.cursor()
		print("SELECT compOnda, irradiancia FROM espectro WHERE datahora_espec >= %s AND datahora_espec < %s AND cidade = %s",[dtTm1,dtTm2,city])
		cur.execute("SELECT compOnda, irradiancia FROM espectro WHERE datahora_espec >= %s AND datahora_espec < %s AND cidade = %s",[dtTm1,dtTm2,city])
		resultSet = cur.fetchall()
		x = []
		y = []
		for row in resultSet:
			x.append(row[0])
			y.append(row[1])
		cur.close()
		con.close()
		return x,y
    
############################################################################
#Cria conexões com o BD
class Connection():

	@staticmethod
	def getConnection():
		return psycopg2.connect(host='localhost', database='dadosatmosfericos',
	user='postgres', password='111791')

############################################################################
class GetSpeecFiles():

	#construtor
	#Entrada: files, cidade
	#Atributos: self.files, self.filteredFiles, self.cidade
    def __init__(self,files, cidade):
        self.files = files
        self.filteredFiles = {}
        self.cidade = cidade

    #Leitura de um arquivo file gerado pelo GetSpec
    #Retorna interpolations, que é uma lista de listas, onde cada lista é uma interpolação
    def getSpectrumFromFile(self,file):
        try:
            fd = open(file,encoding="utf8",errors='ignore')
            interpolations = []
            cols = self.readFileSpectrum_getHeader(fd)
            for line in fd:
                data = line.split(";")
                if(len(data)>=cols["Wave"]) and (len(data)>=cols["Absolute Irradiance"]):
                    if(is_number(data[cols["Wave"]].replace(',','.'))) and (is_number(data[cols["Absolute Irradiance"]].replace(',','.'))):
                        p = [data[cols["Wave"]].replace(',','.').rstrip(),data[cols["Absolute Irradiance"]].replace(',','.').rstrip()]
                        interpolations.append(p)
            fd.close()
            return interpolations
        except UnicodeDecodeError:
            raise
        except TypeError:
            raise

    #Procura em qual coluna estão os campos "Wave" e "Absolute Irradiance"
    #Retorna col_id, um dicionário onde a chave são os campos e o valor o número da coluna
    #Número da coluna inicia-se em 0
    def readFileSpectrum_getHeader(self,fd):
        try:
            col_id = {"Wave":None,"Absolute Irradiance":None}
            cont = 0
            for line in fd:
                cols = line.split(";")
                for i in range(len(cols)):
                    if cols[i].strip() in col_id.keys():
                        col_id[cols[i].strip()] = i
                        cont += 1
                if cont >= len(col_id):
                    break
            return col_id
        except UnicodeDecodeError:
            raise

    #Chama a função self.filterFiles para filtrar arquivos a partir do valor de selectedTime
    #Realiza a leitura dos arquivos em self.filteredFiles
    #Caso um arquivo não tenha uma tupla com mesmo valor de data, hora e cidade, chama Coleta.insertColeta e insere a tupla no BD
    def readFiles(self, selectedTime):
        try:
            self.filterFiles(selectedTime)
            for file in self.filteredFiles.keys():
                print("Reading ")
                print(file)
                dtTm = self.filteredFiles[file]
                print(dtTm)
                interpolacoes = self.getSpectrumFromFile(file)
                if len(interpolacoes) > 0:
                    print("Interpolations quantity:")
                    print(len(interpolacoes))
                    espectro = Spectrum(dtTm,self.cidade,interpolacoes)
                    espectro.filterCurve()
                    if len(espectro.curve)>0:
                        if Coleta.selectColeta(self.cidade,dtTm)<=0:
                            Coleta.insertColeta(self.cidade,dtTm)
                        espectro.insertSpectrum()
        except FileNotFoundError:
            MessageBox("Erro","Diretório não encontrado")
        except UnicodeDecodeError:
            MessageBox("Erro","Arquivo não conseguiu ser decodificado")
            raise
        except TypeError:
            MessageBox("Erro","Arquivo não conseguiu ser interpretado")
            raise

    #Verifica quais arquivos de self.files podem ser lidos
    #Para um arquivo ser lido, ele deve ser .tit, estar dentro do tempo escolhido e possuir um arquivo com mesmo nome em .IRR
    #Data e hora utilizadas para o arquivo .tit é a do arquivo .IRR correspondente
    #Retorno é um dicionário, com chave = arquivo .tit e valor = data e hora do arquivo .iRR correspondente
    def filterFiles(self, tempo):
        fileTimestamp = {}
        filesToRead = {}
        timeZoneSJC = timezone('America/Sao_Paulo')
        try:
            for file in self.files: #Selecionará a data e hora dos arquivos
                nome, ext = os.path.splitext(file)
                if ext == '.IRR':
                    dtTmFile = datetime.datetime.fromtimestamp(file.stat().st_mtime,tz=timeZoneSJC) #Salva a data e hora do arquivo
                    dtTmFile = dtTmFile.replace(second=0, microsecond=0) #Substitui os segundos e microsegundos por falta de precisão
                    dateTimeDifference = datetime.datetime.now(tz = timeZoneSJC) - dtTmFile #Calcula há quanto tempo o arquivo foi criado
                    if dateTimeDifference.total_seconds() < tempo: #Se o tempo é menor do que o selecionado pelo usuário, salva em FileTimstamp
                        fileTimestamp[os.path.splitext(os.path.basename(file))[0]] = dtTmFile

            for file in self.files: #Selecionará os arquivos a serem lidos
                nome, ext = os.path.splitext(file)
                nome = os.path.splitext(os.path.basename(file))[0]
                if ext == '.tit':
                    if nome in fileTimestamp.keys(): #Verifica se o arquivo correspondente em .IRR está em fileTimestamp
                        filesToRead[file] = fileTimestamp[nome] #Se sim, o arquivo deverá ser lido
        except FileNotFoundError:
            MessageBox("Erro","Diretório não encontrado")
        self.filteredFiles = filesToRead

############################################################################################################################
############################################################################################################################
#############################################<                    >#########################################################
#######################################<        INTERFACE GRÁFICA      >####################################################
#############################################<                  >###########################################################
############################################################################################################################
class App(Tk):

	#Contrutor
	#Cria a janela principal e o conteúdo delas
	def __init__(self, *args, **kwargs):
		Tk.__init__(self,*args,**kwargs)

		#Janela
		self.geometry('310x220')
		self.resizable(width=FALSE, height = FALSE)
		self.title("Banco de Dados Espectrais")
		#self.iconbitmap(r'sun.ico')

		#Conteúdo
		container = Frame(self)
		container.pack(side="top",fill="both", expand=True)
		container.place(relx=0.5, rely=0.5, anchor=CENTER)
		container.grid_rowconfigure(0, weight=1)
		container.grid_columnconfigure(0, weight=1)


		#Telas a serem exibidas
		self.frames = {}
		for f in (ScanPage, SearchSpectrumPage):
			frame = f(container, self)
			self.frames[f] = frame
			frame.grid(row=0, column=0,sticky="nsew")

		self.showFrame(ScanPage)

		#Menu superior
		menu = Menu(self)
		self.config(menu = menu)

		#submenu
		subMenu = Menu(menu,tearoff=0)
		menu.add_cascade(label="Opções",menu=subMenu)
		subMenu.add_command(label="Varredura de Pasta", command=lambda: self.showFrame(ScanPage))
		subMenu.add_command(label="Visualizar Espectro", command=lambda:self.showFrame(SearchSpectrumPage))
		subMenu.add_separator()
		subMenu.add_command(label="Sair", command=quit)
		
	#Exibe a tela correspondente a context
	def showFrame(self, context):
		frame = self.frames[context]
		frame.tkraise()

class SearchSpectrumPage(Frame):

	#Construtor
	#Cria o conteúdo da tela
	def __init__(self, master, controller):
		Frame.__init__(self,master)

		#Posição
		searchSpectrumFrame = Frame(self)
		searchSpectrumFrame.grid(row = 1)

		#Labels
		self.lblTitle = Label(self, text="Visualização de Espectro", font='Helvetica 12 bold')
		self.lblTitle.grid(row=0, padx=10, pady=10, columnspan=2)

		#Entries
		citySearch = StringVar()
		citySearch.set("Cidade")
		self.citySearch = Entry(self, textvariable = citySearch, bd = 0)
		self.citySearch.grid(row=2,columnspan=2, padx=30, pady=10, sticky="nsew")

		#Entradas para data e hora
		self.day = Entry(searchSpectrumFrame, width=2, bd = 0)
		self.label_1 = Label(searchSpectrumFrame, text="/")
		self.month = Entry(searchSpectrumFrame, width=2, bd = 0)
		self.label_2 = Label(searchSpectrumFrame, text="/")
		self.year = Entry(searchSpectrumFrame, width=4, bd = 0)
		self.label_3 = Label(searchSpectrumFrame, text=" ")
		self.hour = Entry(searchSpectrumFrame, width=2, bd = 0)
		self.label_4 = Label(searchSpectrumFrame, text=":")
		self.minute = Entry(searchSpectrumFrame, width=2, bd = 0)
		self.label_5 = Label(searchSpectrumFrame, text=":")
		self.second = Entry(searchSpectrumFrame, width=2, bd = 0)
		self.label_6 = Label(searchSpectrumFrame, text=":")
		self.milisec = Entry(searchSpectrumFrame, width=10, bd = 0)

		self.day.grid(row=1, column = 0)
		self.label_1.grid(row=1, column = 1)
		self.month.grid(row=1, column = 2)
		self.label_2.grid(row=1, column = 3)
		self.year.grid(row=1, column = 4)
		self.label_3.grid(row=1, column = 5)
		self.hour.grid(row=1, column = 6)
		self.label_4.grid(row=1, column = 7)
		self.minute.grid(row=1, column = 8)
		self.label_5.grid(row=1, column = 9)
		self.second.grid(row=1, column = 10)
		self.label_6.grid(row=1, column = 11)
		self.milisec.grid(row=1, column = 12)

		self.entries = [self.day, self.month, self.year,self.hour, self.minute, self.second,self.milisec]

		self.day.bind('<KeyRelease>', lambda e: self._check(0, 2))
		self.month.bind('<KeyRelease>', lambda e: self._check(1, 2))
		self.year.bind('<KeyRelease>', lambda e: self._check(2, 4))
		self.hour.bind('<KeyRelease>', lambda e: self._check(3, 2))
		self.minute.bind('<KeyRelease>', lambda e: self._check(4, 2))
		self.second.bind('<KeyRelease>', lambda e: self._check(5, 2))
		self.milisec.bind('<KeyRelease>', lambda e: self._check(6, 10))

		self.dtTm = ""
		#Buttons
		self.searchBtn = Button(self, text="Buscar",command=lambda:thread.Thread(target=self.searchSpectrum()), border=3, width=17)
		self.searchBtn.grid(row = 3, columnspan=2, padx=30, pady=10)

	#Método referente a entrada de data e hora
	def _backspace(self, entry):
		cont = entry.get()
		entry.delete(0, END)
		entry.insert(0, cont[:-1])

	#Método referente a entrada de data e hora
	def _check(self, index, size):
		entry = self.entries[index]
		next_index = index + 1
		next_entry = self.entries[next_index] if next_index < len(self.entries) else None
		data = entry.get()

		if len(data) > size or not data.isdigit():
			self._backspace(entry)
		if len(data) >= size and next_entry:
			next_entry.focus()
		
		self.dtTm = "{}-{}-{} {}:{}:{}.{}".format(self.year.get(),self.month.get(), self.day.get(),self.hour.get(), self.minute.get(), self.second.get(),self.milisec.get())
		print(self.dtTm)

	#Método referente a entrada de data e hora
	def get(self):
		return [e.get() for e in self.entries]  

	#Retorna dtTime1 que é value e dtTime2 que é um minuto mais que dtTime1
	def defineInterval(self,value): 
		dtTime1 = datetime.datetime.strptime(value, "%Y-%m-%d %H:%M:%S.%f").replace(microsecond=0)
		dtTime2 = dtTime1 + datetime.timedelta(0,1)
		dtTime2 = dtTime1.replace(minute=dtTime1.minute+1)
		print(dtTime1)
		print(dtTime2)
		return dtTime1, dtTime2  
		
	#Método do botão Buscar 
	#Procura por dados na tabela espectro no BD em que cidade = self.citySearch.get() e datahora_espec = self.dtTm
	#Caso não existam dados com esses valores, cria um intervalor a partir de self.dtTm e realiza a busca novamente
	#Se dados forem retornados cria um gráfico
	def searchSpectrum(self):
		try:
			if is_TimestampPast(self.dtTm):
				x,y = Spectrum.selectSpectrum(self.citySearch.get(), self.dtTm)
				if len(x)!=0:
					self.createGraphics(x,y) #Se há interpolações cria o gráfico
				else: #Senão procura dentro de um intervalo de tempo
					dtTime1, dtTime2 = self.defineInterval(self.dtTm)
					decision = MessageBoxYesNo("Fim da operação","Não há dados espectrais para a Data e Hora = %s e Cidade = %s.\nGostaria de pesquisar por dados entre %s e %s"%(self.dtTm, self.citySearch.get(),dtTime1, dtTime2))
					if decision:
						x,y = Spectrum.selectSpectrumBetweenInterval(self.citySearch.get(), dtTime1, dtTime2)
						if len(x)!=0:
							self.createGraphics(x,y)
						else:
							MessageBox("Fim da nova operação","Não foram encontrado dados espectrais para o intervalo.")
			else:
				MessageBox("Erro","Preencha uma data e/ou hora válida!")
		except ValueError:
			MessageBox("Erro","Preencha uma data e/ou hora válida!")
		except:
			MessageBox("Erro","Ocorreu algum erro durante a operação.")

	#Cria gráfico
	def createGraphics(self,x,y):
		fig, ax = plt.subplots()
		ax.plot(x, y)
		ax.set(xlabel='Comprimento de Onda (nm)', ylabel='Irradiância (microW/cm² nm)', title='Espectro da Radiação Solar')
		ax.grid()
		#fig.savefig("test.png")
		plt.show()	

class ScanPage(Frame):

	#Contrutor
	def __init__(self, master, controller):
		Frame.__init__(self,master)

		#Labels
		self.lblTitle = Label(self, text="Leitura de Arquivos Espectrais", font='Helvetica 12 bold')
		self.lblTitle.grid(row=0, padx=10, pady=10, columnspan=2)

		self.lblDiretorio = Label(self, text="Diretório:")
		self.lblDiretorio.grid(row=1, padx=10, pady=5, column=0, sticky=E)
		self.lblDiretorio.bind("<Enter>", self.on_enter)
		self.lblDiretorio.bind("<Leave>", self.on_leave)

		self.lblInitTime = Label(self, text="Tempo de criação:")
		self.lblInitTime.grid(row=2,  padx=10, pady=5, column=0, sticky=E)

		#Buttons
		self.searchBtn = Button(self, text="Procurar",command=self.searchFolder, border=1, width=13)
		self.searchBtn.grid(row=1, padx=10, pady=5, column=1)

		self.enterBtn = Button(self, text="OK", command=self.enterBtn, border=1, width=17)
		self.enterBtn.grid(row=6, columnspan=2, column=0, padx=10, pady=5)

		#Entries
		cidade = StringVar()
		cidade.set("Cidade")
		self.cidade = Entry(self, textvariable = cidade, bd = 0)
		self.cidade.grid(row=3, padx=10, pady=10, columnspan=2)

		#DropDown List
		optionsList = ["1 dia atrás", "1 semana atrás", "30 dias atrás", "Todos os arquivos"]
		inicio = StringVar()
		inicio.set(optionsList[0])
		self.Option = OptionMenu(self,inicio,*optionsList)
		#self.Option.config(width=9)
		self.Option.grid(row=2,column=1)


	def on_enter(self, event):
		self.lblDiretorio.configure(text="Hello world")

	def on_leave(self, enter):
		self.lblDiretorio.configure(text="")

	#Fução do botão para procurar por diretório a ser analisado
	#Diretório armazenado em self.Diretorio
	def searchFolder(self):
		folder = filedialog.askdirectory(title="Selecione uma pasta")
		if folder != "":
			self.lblDiretorio['text'] = ".."+folder[-10:]
			self.Diretorio = folder

	#Função recursiva que procura em cada diretório os arquivos que elas armazenam
	#Arquivos a serem lidos são armazenados na lista filesToRead
	#Retorna filesToRead
	def getAllFilesFromFolder(self,path):
		filesToRead = []
		with os.scandir(path) as listOfEntries:
			for entry in listOfEntries:
				if entry.is_file():
					filesToRead.append(entry)
				else:
					filesToRead += self.getAllFilesFromFolder(entry)
		return filesToRead
		
	#Função do botão OK
	#chama self.getAllFilesFromFolder para procurar todos os arquivos em self.Diretorio
	#instancia a classe do tipo de arquivo que irá ler
	#realiza a leitura dos arquivos
	def enterBtn(self):
		intervals = {"1 dia atrás":86400, "1 semana atrás":604800, "30 dias atrás":2592000, "Todos os arquivos":float("inf")}
		files = self.getAllFilesFromFolder(self.Diretorio)
		precessor = GetSpeecFiles(files,self.cidade.get())
		precessor.readFiles(intervals[self.Option['text']])
		MessageBox("","Fim da Operação")


################################################################################

#Dado um valor s verifica se é número, retorna True, ou não, retorna False
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
 
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    return False

#Dado uma data e hora value, verifica se é tempo passado ou agora, retorna True, ou não, retorna False
def is_TimestampPast(value):
	try:
		dtTmValue = datetime.datetime.strptime(value, "%Y-%m-%d %H:%M:%S.%f")
		dtTmNow = datetime.datetime.now()
		if (dtTmValue < dtTmNow) or (dtTmValue == dtTmNow):
			return True
		else:
			return False
	except ValueError:
		raise

#Cria caixas de mensagens com título = title e corpo de menssagem = message
def MessageBox(title, message):
	t = thread.Thread(target=ctypes.windll.user32.MessageBoxW, args=(0, message, title, 0))
	t.start()

#Cria caixas de mensagens com título = title e corpo de menssagem = message
#Caixa de mensagem com botões do tipo Yes, retorna True, e No, retorna False
def MessageBoxYesNo(title, message):
	answer = ctypes.windll.user32.MessageBoxW(0, message, title, 4)
	if answer == 6:
		return True
	else:
		return False
################################################################################
############################## PRINCIPAL  ######################################
################################################################################
################################################################################
app = App()
app.mainloop()


